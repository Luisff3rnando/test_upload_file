from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import os
from dotenv import load_dotenv
from sqlalchemy.orm import declarative_base

load_dotenv()


connect_args = {
    'init_command': "SET @@collation_connection='latin1_spanish_ci'"}


connect = os.environ.get('MYSQL_URI', 'test')
engine = create_engine(
    connect, connect_args=connect_args,
    echo=False,
    pool_size=20,
    max_overflow=0)
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()
