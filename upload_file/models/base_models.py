from sqlalchemy import Column
from sqlalchemy import Boolean
from sqlalchemy import DateTime
from sqlalchemy import func


from config import conexion

# class GlobalManager(conexion.Base.Manager):
#     def get_queryset(self):
#         return super().get_queryset().filter(active=True)


class BaseModel(conexion.Base):
    __abstract__ = True
    active = Column(Boolean, default=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    deleted_at = Column(DateTime(timezone=True), onupdate=func.now())
