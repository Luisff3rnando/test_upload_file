from uuid import uuid4
from sqlalchemy import Column
from sqlalchemy import String
from sqlalchemy import Text
from sqlalchemy import DateTime
from sqlalchemy import Integer


# base model
from .base_models import BaseModel


def generate_uuid():
    return str(uuid4())


class File(BaseModel):
    __tablename__ = 'files'
    id = Column(Integer, primary_key=True)
    uuid = Column(Text, default=generate_uuid)
    file = Column(Text)
    nid = Column(String(128))
    phone = Column(String(128))
    nit = Column(String(128))
    address = Column(String(128))
    invoice = Column(String(128))
    value = Column(String(128))
    expedition_date = Column(DateTime(timezone=True))

    def __repr__(self):
        return "<File(uuid='%s', nid='%s', phone='%s')>" % (
            self.uuid, self.nid, self.phone)
