import json

import pytest
from rest_framework import status


class TestFileAPIView:
    endpoint = '/api/v1/file'

    @pytest.mark.django_db
    def test_post(self, api_client):
        response = api_client().get(self.endpoint)

        assert response.status_code == status.HTTP_201_OK
        assert len(json.loads(response.content)) == 5

    @pytest.mark.django_db
    def test_get(self, api_client):
        params = dict(
            nid="10101011",
            page="1",
            per_page="3",
            order="ASC",
            field_order="nid",
            phone="314559777"
        )

        response = api_client().post(self.endpoint,
                                     params=params, format='json')
        assert response.status_code == status.HTTP_201_CREATED
        assert len(json.loads(response.content)) == 3
