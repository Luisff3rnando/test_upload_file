from drf_yasg import openapi
from drf_yasg.openapi import Schema
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

# serializers
from upload_file.serializers.serializers import FileSerializers
from upload_file.serializers.serializers import FileFilterSerializers
from upload_file.serializers.serializers import FileModelSerializers

# # handlers
from app.core.handler import HandlerFile


class FileView(APIView):
    @swagger_auto_schema(
        responses=None,
        request_body=FileSerializers,
        tags=['File']
    )
    def post(self, request, format=None):
        """
        Upload file
        """

        try:
            serializer = FileSerializers(data=request.data)
            if not serializer.is_valid():
                return Response(
                    dict(success=False,
                         code=400,
                         message="Not valid request: " +
                                 str(serializer.errors),
                         status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                )
            HandlerFile.create(
                serializer.initial_data)
            return Response(
                dict(message="OK",
                     data="OK",
                     code=201),
                status=status.HTTP_201_CREATED
            )
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )

    @swagger_auto_schema(
        responses={200: FileModelSerializers()},
        request_body=None,
        operation_description="Get List Documents",
        tags=['File'],
    )
    def get(self, request, format=None):
        """
        parameters:
        -  nid
        -  page=1
        -  per_page=3
        -  order=ASC&
        -  field_order=nid
        -  phone=
        """
        try:
            serializer = FileFilterSerializers(data=request.query_params)
            if not serializer.is_valid():
                return Response(
                    dict(success=False,
                         code=400,
                         message="Not valid request: " +
                         str(serializer.errors),
                         status=status.HTTP_422_UNPROCESSABLE_ENTITY)
                )
            handler = HandlerFile.get_one(serializer.data)
            return Response(
                dict(success=False,
                     code=0,
                     message="OK",
                     data=handler
                     ),
                status=status.HTTP_200_OK)
        except Exception as e:
            return Response(
                dict(success=False,
                     code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                     message=str(e)),
                status=status.HTTP_422_UNPROCESSABLE_ENTITY
            )


class HealthView(APIView):
    @swagger_auto_schema(
        operation_description="Verify status of service",
        tags=["health"],
        operation_summary="API health check service",
        responses={
            200: 'Service is Ok'
        }
    )
    def get(self, request):
        return Response(
            dict(status="ok")
        )
