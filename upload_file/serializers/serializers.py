from rest_framework import serializers


class FileSerializers(serializers.Serializer):
    reports = serializers.FileField()
    user_id = serializers.IntegerField()


class FileFilterSerializers(serializers.Serializer):
    page = serializers.IntegerField()
    per_page = serializers.IntegerField()
    order = serializers.CharField()
    field_order = serializers.CharField()

    id = serializers.CharField(required=False)
    uuid = serializers.CharField(required=False)
    file = serializers.CharField(required=False)
    nid = serializers.CharField(required=False)
    phone = serializers.CharField(required=False)
    nit = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    invoice = serializers.CharField(required=False)
    value = serializers.CharField(required=False)
    expedition_date = serializers.CharField(required=False)


class FileModelSerializers(serializers.Serializer):

    id = serializers.CharField()
    uuid = serializers.CharField()
    file = serializers.CharField()
    nid = serializers.CharField()
    phone = serializers.CharField()
    nit = serializers.CharField()
    address = serializers.CharField()
    invoice = serializers.CharField()
    value = serializers.CharField()
    expedition_date = serializers.CharField()
