from django.urls import path

from .views.views import FileView

urlpatterns = [
    path(
        route='file',
        view=FileView.as_view(),
        name='upload files'
    )
]
