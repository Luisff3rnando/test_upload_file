import threading

list = ["abc", "mmm", "pppp", "oooo", "llll", "qqqq"]


def worker():
    """thread worker function"""
    print('Worker')


threads = []
for i in range(len(list)):
    t = threading.Thread(target=worker)
    threads.append(t)
    t.start()
