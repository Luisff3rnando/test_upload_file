from abc import ABC
from abc import abstractmethod


class Mixin(ABC):

    @abstractmethod
    def save(self, kwargs):
        pass

    @abstractmethod
    def get(self, kwargs):
        pass

    @abstractmethod
    def update(self, kwargs):
        pass

    @abstractmethod
    def delete(self, kwargs):
        pass

    @abstractmethod
    def filter(self, kwargs):
        pass
