from sentry_sdk import capture_exception
from sqlalchemy import desc
from sqlalchemy_paginator import Paginator
from .mixin_queryset import Mixin
from .exceptions import FileNotFound
from .exceptions import GeneralException

from config.conexion import session


class GenericQueryset(Mixin):

    def __init__(self, entity):
        self.entity = entity

    def save(self, data):
        try:
            _instance = self.entity(**data)
            session.add(_instance)
            session.commit()
            return _instance

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise GeneralException(e)
        finally:
            session.close()

    def get(self,
            data,
            page,
            per_page,
            order,
            field_order,
            message=None):
        """
        Get one data include filters, order_by, columns to orders
        """
        try:
            # _order_by = "{}.{}.{}{}".format(
            #     self.entity, field_order, order, ())

            _instance = session.query(self.entity).filter_by(
                **data).order_by()

            _paginator = Paginator(_instance, per_page)
            _page = _paginator.page(page)

            meta = {
                "data": "",
                "current_page": str(_page),
                "item_per_page": per_page,
                "total_page": _page.paginator.total_pages,
                "available_orders": [
                    "ASC",
                    "DESC"
                ]
            }
            return meta, _page.object_list,

        except Exception as e:
            session.rollback()
            capture_exception(Exception)
            raise FileNotFound(e)

    def filter(self, data, message=None):
        try:
            _instance = session.query(self.entity).filter_by(
                **data)
            return _instance

        except Exception:
            session.rollback()
            capture_exception(Exception)
            raise Exception("{} not found".format(message))

        finally:
            session.close()
            raise Exception("{} not found".format(message))

    def update(self, data, filter, message=None):
        pass

    def delete(self, data, message=None):
        pass
