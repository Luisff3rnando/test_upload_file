from abc import ABC
from abc import abstractmethod


class Crud(ABC):

    @abstractmethod
    def create(self, kwargs):
        pass

    @abstractmethod
    def filter(self, kwargs):
        pass

    @abstractmethod
    def get_one(self, kwargs):
        pass
