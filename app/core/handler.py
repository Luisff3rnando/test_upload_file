
from app.core.process import Files


class HandlerFile:
    @staticmethod
    def create(kwargs):
        result = Files().create(kwargs)
        return result

    @staticmethod
    def get_one(kwargs):
        result = Files().get_one(kwargs)
        return result
