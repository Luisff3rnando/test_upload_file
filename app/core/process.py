# from sentry_sdk import capture_exception

import os
import threading
import time
from upload_file.models.file import File
from upload_file.serializers.serializers import FileModelSerializers
# interface
from .mixin_crud import Crud
# queryset
from .queryset import GenericQueryset
# utils
from .utils import validate_extension
from .utils import generate_file_name
from .utils import convert_to_json
from .utils import http_request
from .exceptions import GeneralException

# s3
from .utils import MediaStorage


class Files(Crud):
    """
    """

    def upload_file(self, kwargs):

        file_obj = kwargs

        file_name = generate_file_name(file_obj.name,)
        file_path_bucket = os.path.join(
            "/",
            file_name
        )

        media_storage = MediaStorage()

        try:
            if not media_storage.exists(file_path_bucket):
                media_storage.save(file_path_bucket, file_obj)
                file_url = media_storage.url(file_path_bucket)

                # realiza peticion http al archivo s3 retorna I/O
                object_file_s3 = http_request(file_url)
                return file_url, object_file_s3
            raise
        except Exception as e:
            raise GeneralException(e)

    def create(self, kwargs):
        """
        validate that the file format is csv,
        execute the function to load the document

        :: validate_extension

        :: upload_file

        :: convert_to_json

        :: save data
        """
        try:
            file = kwargs.get('reports')
            # valida que la extension sea permitida
            # de lo contrario retorna una excepcion
            validate_extension(kwargs.get('reports'))

            # cargar archivo
            url_s3, data_csv = self.upload_file(file)

            # convierte el dataframe a Json
            df_json = convert_to_json(data_csv)

            for index in df_json:
                time.sleep(0.1)
                new_data = df_json[index]
                new_data['file'] = url_s3
                GenericQueryset(File).save(df_json[index])

            return
        except Exception as e:
            raise GeneralException(e)

    # def thread(self, data):
    #     threads = []
    #     for i in range(len(list)):
    #         t = threading.Thread(target=self.create())
    #         threads.append(t)
    #         t.start()

    def get_one(self, kwargs):
        """
        Get one and filter
        """
        # data,
        page = kwargs.get('page')
        kwargs.pop('page')
        per_page = kwargs.get('per_page')
        kwargs.pop('per_page')
        order = kwargs.get('order')
        kwargs.pop('order')
        field_order = kwargs.get('field_order')
        kwargs.pop('field_order')

        paginator, data = GenericQueryset(File).get(data=kwargs,
                                                    page=page,
                                                    per_page=per_page,
                                                    order=order,
                                                    field_order=field_order)

        response = FileModelSerializers(data, many=True).data
        paginator['data'] = response
        return paginator

    def filter(self):
        pass
