# from .contants import COLUMNS
from storages.backends.s3boto3 import S3Boto3Storage
import os
import uuid
import json
from io import StringIO
import requests
import pandas as pd
from dotenv import load_dotenv
from sentry_sdk import capture_exception

load_dotenv()


def validate_extension(file):
    allow_extension = ['text/csv']

    if file.content_type in allow_extension:
        return
    else:
        raise Exception("extension not permitida")


def generate_file_name(file_name):
    """ Random name for files """
    name_rev = file_name[::-1].find('.')
    name_ext = file_name[len(file_name)-name_rev:]
    final = "{}.{}".format(str(uuid.uuid4()), name_ext)
    return final


def convert_to_json(kwargs):
    try:
        df = pd.read_csv(kwargs, delimiter=';')

        # clean data ordeer dataframe
        df_to_json = df.to_json(orient='index')
        to_json = json.loads(df_to_json)

        return to_json
    except Exception as e:
        raise Exception(e)


def http_request(url, params: dict = None):
    """
    Crea una peticion al archivo ubicado en s3, parsea
    la data a I/O, para ser leida desde panadas
    """
    try:

        headers = {
            'Content-Type': 'application/json',
        }

        response = requests.get(
            url,
            headers=headers
        )
        response = str(response.content, 'utf-8')
        response = StringIO(response)
        return response
    except Exception as e:
        capture_exception(e)
        raise Exception(str(e))


class MediaStorage(S3Boto3Storage):
    bucket_name = os.getenv("AWS_STORAGE_BUCKET_NAME", str)
